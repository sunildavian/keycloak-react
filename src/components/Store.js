class Store {

    constructor() {

        this.keycloak = void 0;

    }

    setKeyCloak = (keycloak) => {
        this.keycloak = keycloak;
    }

    getKeyCloak = () => {
        return this.keycloak;
    }

    updateLocalstorage = () => {
        localStorage.setItem("kc_token", this.keycloak.token)
        localStorage.setItem("kc_refreshToken", this.keycloak.refreshToken)
    }
}

export default Store;