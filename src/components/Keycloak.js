import React, {Component} from "react";
import {Link} from "react-router-dom";
import Keycloak from "keycloak-js";
import Store from "./Store";
import Welcome from "./Welcome";

class Login extends Component {

    constructor() {
        super();
        this.store = new Store();
        this.keycloak = Keycloak('/keycloak.json');
        this.state = {
            auth: void 0
        }
    }

    componentDidMount() {

        this.keycloak.init({onLoad: 'login-required'})
            .then(authenticated => {
                if (authenticated) {
                    this.store.setKeyCloak(this.keycloak);
                    this.store.updateLocalstorage();
                    this.setState({auth: true})
                }
            });

    }


    render() {

        if (this.state.auth) {
            return (<div>
                <h1>Welcome to 360</h1>
                <Welcome store={this.store}/>
            </div>);
        } else {
            return (<div>
                PLease Wait Loading..............
            </div>)
        }

    }
}

export default Login;