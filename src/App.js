import React, { Component } from 'react';
import logo from './logo.svg';
import {Route} from "react-router-dom";
import './App.css';
import Homepage from "./components/Homepage"
import login from "./components/login"

class App extends Component {
  render() {
    return (
      <div className="App">
        <Route exact strict path={"/"} component={Homepage} />
        <Route exact strict path={"/login"} component={login} />
      </div>
    );
  }
}

export default App;
