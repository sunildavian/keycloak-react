import React, {Component} from "react";

class Welcome extends Component {


    constructor(props) {
        super(props);
        this.state = {
            data: void 0
        }
    }


    fetchOrganization = (e) => {
        e.preventDefault();
        this.fetchData("http://localhost:8090/getOrganization", "POST");
    }

    modifyData = (value, operation) => {

        let url = `http://localhost:8090/modifyOrganization`;
        let method = operation === "delete" ? "DELETE" : "PUT";
        let body = {};
        if (method === "DELETE") {
            body["_id"] = value._id;
        }
        if(method!=="PUT"){

            this.fetchData(url, method, body)
        }else {
            alert("Method not supported.It will come soon");
        }

    }

    fetchData = async (url, method, body = {}) => {

        try {
            let result = await fetch(url, {
                method,
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem("kc_token")}`,
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify(body)
            });

            let resultMessage = await result.json();
            if (resultMessage && resultMessage.status !== "error") {
                this.setState({
                    data: resultMessage
                })
            } else {
                alert(resultMessage && resultMessage.message);
            }

        } catch (err) {
            alert(err.message);
        }

    }

    logout=()=>{

        let keycloak = this.props && this.props.store && this.props.store.getKeyCloak();

        try{

            if(keycloak){
                keycloak.logout();
                localStorage.clear();
            }

        }catch (e) {
            localStorage.clear();
        }

    }

    render() {
        let {data} = this.state;
        return (<div>

            <h2>Welcome page</h2>

            <div><button onClick={this.logout}>logout</button></div>

            <br/>


            <button onClick={this.fetchOrganization}>Show Organization</button>
            &nbsp;
            <button onClick={this.addOrganization}>Add Organization</button>

            <div>
                {data &&
                <ul>
                    {data.map((value, index) => (
                        <div>
                            <li key={index}>{value.name}</li>
                            <button onClick={(e) => {
                                this.modifyData(value, "delete")
                            }}>Delete
                            </button>
                            &nbsp;
                            <button onClick={(e) => {
                                this.modifyData(value, "update")
                            }}>Update
                            </button>
                        </div>
                    ))}
                </ul>
                }
            </div>
        </div>);
    }
}

export default Welcome;