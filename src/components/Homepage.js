import React, {Component} from "react";
import {Link} from "react-router-dom";

class Homepage extends Component{


    render(){
        return (<div>
            <h1>Welcome to 360</h1>

            <button>
                <Link to={"/login"}>Login</Link>
            </button>
        </div>);
    }
}

export default Homepage;